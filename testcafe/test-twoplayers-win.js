import { Selector } from 'testcafe';
import { namePlayer, randomClick } from './commons';

fixture `Two players`
    .page `http://localhost:8080/`;

test('Two players match', async t => {

    await namePlayer();

    //No check for matchMaking here, as this page is not displayed if another player is already ready

    await t
        .expect(Selector('.back-layer').exists).ok()
        .expect(Selector('.front-layer').exists).ok()
        .expect(Selector('.SignScore').exists).ok()
        //Check the game div now exists


    const stareOpacity = Selector('.Stare').getStyleProperty('opacity');
    const exclamationOpacity = Selector('.Exclamation').getStyleProperty('opacity');

    await t
    .expect(exclamationOpacity).eql('0')
    .expect(stareOpacity).eql('1');

    await t
      .expect(stareOpacity).eql('0')
      //No attempt to click before the exclamation mark shows up in order for the test to pass!
      await t.expect(exclamationOpacity).eql('1', {timeout: 6000 });
      //Wait for the game to start. Timeout is set to 6000, as it's the longest time to wait before fighting !

      await randomClick();

    await t.expect(await Selector('.sign.no-tie').clientHeight).gte(0);
    await t.expect(await Selector('.sign.winner').clientHeight).gte(0);
    //Check that the sign are displayed

    await t.expect(Selector('.SignScore').textContent).notMatch(/^00/);
    //Check the scrore is not 00

    await t
      .expect(Selector('.SceneEndSlide').exists).ok()
      .expect(Selector('.SceneThanks').exists).ok()


    // await randomClick();
    // await t
    //     .expect(Selector('.back-layer').exists).ok()
    //     .expect(Selector('.front-layer').exists).ok()
    //     .expect(Selector('.SignScore').exists).ok()
    //     //Check the game div now exists


});
