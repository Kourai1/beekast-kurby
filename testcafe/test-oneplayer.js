import { Selector } from 'testcafe';
import { namePlayer, randomClick } from './commons';

fixture `One player`
    .page `http://localhost:8080/`;

test('Only Player cannot play', async t => {

    await namePlayer();

    const matchMakingDiv = Selector('.SceneMatchMaking');

    await t
        .expect(matchMakingDiv.exists).ok()
        .expect((matchMakingDiv.withText('Searching match')).exists).ok()
        .expect((matchMakingDiv.withText('1 player(s) online')).exists).ok()
        .expect((matchMakingDiv.withText('2 player(s) online')).exists).notOk()
        //Check the matchMaking div exists, and the text show only one player

    await t
        .expect(Selector('.back-layer').exists).notOk()
        .expect(Selector('.front-layer').exists).notOk()
        .expect(Selector('.SignScore').exists).notOk()
        .expect(matchMakingDiv.exists).ok();
        // Check none of the game div exists, and check again that we are still in match making mode

    await randomClick();
    await t
        .expect(Selector('.back-layer').exists).notOk()
        .expect(Selector('.front-layer').exists).notOk()
        .expect(Selector('.SignScore').exists).notOk()
        .expect(matchMakingDiv.exists).ok();
        // No Change after a random click
});
