import { Selector } from 'testcafe';
import { namePlayer, randomClick } from './commons';

fixture `Two players`
    .page `http://localhost:8080/`;


test('Two players match', async t => {

    await namePlayer();

    //No check for matchMaking here, as this page is not displayed if another player is already ready

    await t
        .expect(Selector('.back-layer').exists).ok()
        .expect(Selector('.front-layer').exists).ok()
        .expect(Selector('.SignScore').exists).ok()
        //Check the game div now exists

    const stareOpacity = Selector('.Stare').getStyleProperty('opacity');
    const exclamationOpacity = Selector('.Exclamation').getStyleProperty('opacity');

    await t
    .expect(exclamationOpacity).eql('0')
    .expect(stareOpacity).eql('1');

    await randomClick();

    await t
      .expect(await Selector('.sign.no-tie').clientHeight).eql(0);
    await t
      .expect(await Selector('.sign.winner').clientHeight).eql(0);
    //Check that the stares shows up, and that you cannot win for now.
    //we need to await to get DOM node state.

    await t
      .expect(stareOpacity).eql('0')
      .expect(exclamationOpacity).eql('1', {timeout: 6000 });

});
