# Testing the Game

## Installing testcafe

testcafe is now referenced in `package.json`, so just run

```
npm install
```

## Testing with one player

First, be sure you don't have another window or tab in any browser with the game on ! Otherwise, there will be more than one player, and the test will fail !

```
npx testcafe chrome testcafe/test-oneplayer.js
```

or

```
npx testcafe firefox testcafe/test-oneplayer.js
```

You can also use any other browser listed in [testcafe browser support list](https://devexpress.github.io/testcafe/documentation/using-testcafe/common-concepts/browsers/browser-support.html)


## Testing with two players

To run test for two player, you can either manually open the game and log in ONE player, then launch the test

```
npx testcafe chrome testcafe/test-twoplayers-XXX.js
```

Or, you can close all your games, and launch the test twice on two different browsers which will compete against one another.

```
npx testcafe chrome,firefox testcafe/test-twoplayers-XXX.js

npx testcafe -c 2 chrome testcafe/test-twoplayers-XXX.js
```

You should also be able to invoke two Chrome instances and runs tests concurrently.
(but it fail on my computer as testcafe launch two browser instances, but run the test in only one, never doing anything on the other...)


### Two players fail

The 1st two players test is `test-twoplayers-fail`, which try to click before the exclamation mark shows up.

```
npx testcafe chrome,firefox testcafe/test-twoplayers-fail.js
```

As you click too quickly the game should restart (as written in the Samurai Kirby wiki), or at least display a message or something. It's not the case right now, you can actually click before the exclamation mark shows up, and win that way.

This test try to click before the exclamation mark shows and check the winning images are not displayed.
So it will fail every time as the game does not act as it should.

This test is for 'demo' purpose only, and should be merge with `test-twoplayers-win` in the real world after filling a bug ticket.


### Two players win

The 2nd two player test is `test-twoplayers-win`, which will click after the exclamation mark is displayed.

```
npx testcafe chrome,firefox testcafe/test-twoplayers-win.js
```

The test will check the score is not 00, and that the winning image is displayed.

The specifications says that you are supposed to go back to the game after clicking on a button, but I never find that button...
the last commented line in the test try to click on the screen and check if we go back to the game. But as it doesn't really fit the specifications (and fail the test), I didn't want to include them.
