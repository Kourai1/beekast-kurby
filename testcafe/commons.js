import { ClientFunction,  Selector, t } from 'testcafe';


//name a player
export async function namePlayer(){

  const registerDiv = Selector('.SceneRegisterUser');
  var playerName = 'player without browser';
  if (t.browser.name == 'Chrome')
  playerName = 'Player Chrome'+Math.floor(Math.random() * 10);

  if (t.browser.name == 'Firefox')
  playerName = 'Player ff'+Math.floor(Math.random() * 10);
  //Give a name based on browser and a random number


    await t
        .expect(registerDiv.exists).ok()
        //Check the div to input name exist

        .typeText(registerDiv.child('input'), playerName)
        .click(registerDiv.child('button'))
        .expect(registerDiv.exists).notOk();
        //type name, validate, and check that the naming div doesn't show anymore
}

//click on a random place on the screen
export async function randomClick(){

    const windowWidth = ClientFunction(() =>  window.innerWidth)
    const windowHeight = ClientFunction(() =>  window.innerHeight)
    const randomX = Math.floor(Math.random() * await windowWidth())
    const randomY = Math.floor(Math.random() * await windowHeight())

    await t
      .click(Selector('body'), {offsetX : randomX, offsetY:randomY })
}
