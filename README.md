_Les autres README servent uniquement si vous avez besoin de lancer le front et le serveur en toute autonomie_

## Instructions pour bien démarrer ( ^_^)／

Tout d'abord installez le jeu:

```
npm install
```

Puis faites un:

```
npm run gameinstall
```


## Instructions pour lancer le jeu 

```
npm run start
```

Le front se lance sur http://localhost:8080... si tout va bien •ᴗ•.

Voila. 
That's all.
Happy Testing.


---

**Ne vous inquietez pas si le projet build avec un warning "BaseStare has unused export property 'class'". C'est normal ( ͡° ͜ʖ ͡°)**
---

Hop hop c'est parti !